# GitLab License management changelog

## v1.2.1

- Better support for js npm projects (!14)

## v1.2.0

- Bump LicenseFinder to 5.5.2

## v1.1.0

- Allow `SETUP_CMD` to skip auto-detection of build tool

## v1.0.0

- Initial release
