FROM licensefinder/license_finder:5.5.2
MAINTAINER GitLab

RUN npm install npm-install-peers cheerio

# Don't let Rubygem fail with the numerous projects using PG or MySQL, install realpath
RUN apt-get update && apt-get install -y libpq-dev libmysqlclient-dev realpath && rm -rf /var/lib/apt/lists/*

# Don't load RVM automatically, it doesn't work with GitLab-CI
RUN mv /etc/profile.d/rvm.sh /rvm.sh

COPY test /test
COPY run.sh html2json.js /

ENTRYPOINT ["/run.sh"]
